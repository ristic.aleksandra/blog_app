import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store/store'

import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'

import './sass/main.scss'

import MainPage from './layout/containers/MainPage'

import ModalContextProvider  from '../src/layout/context/Context'

ReactDOM.render(
    <Provider store={store}>
        <ModalContextProvider>
            <React.Suspense>
                <MainPage />
            </React.Suspense>
        </ModalContextProvider>
    </Provider>,
    document.getElementById('react-app')
)
