import blogImageSrc from '../public/images/blogImg.jpg'
import blogDescriptionImgSrc from '../public/images/blogDescriptionImg.jpg'

const blogDescriptionImgsSrc = [
    {
        src: blogDescriptionImgSrc,
        id: 1,
    },
    {
        src: blogDescriptionImgSrc,
        id: 2,
    },
    {
        src: blogDescriptionImgSrc,
        id: 3,
    },
]

const navItems = [
    { id: 1, name: 'Link1' },
    { id: 2, name: 'Link2' },
    { id: 3, name: 'Link3' },
    { id: 4, name: 'MyProfile'},
    { id: 5, name: 'Logout' },
]

export { blogImageSrc, blogDescriptionImgsSrc, navItems }
