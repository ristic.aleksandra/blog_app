import React, { useState } from 'react'
import PropTypes from 'prop-types'

export const ModalContext = React.createContext({})

const ModalContextProvider = ({ children }) => {
    const [isOpen, setOpen] = useState(false)

    const openModalHandler = (open) => {
        setOpen(open)
    }

    return (
        <ModalContext.Provider
            value={{
                setModalOpen: openModalHandler,
                isModalOpen: isOpen
            }}
        >
            {children}
        </ModalContext.Provider>
    )
}

ModalContextProvider.propTypes = {
    children: PropTypes.any,
}

export default ModalContextProvider
