import React from 'react'
import MainPageHeader from './MainPageHeader'
import MainPageBody from './MainPageBody'
import Modal from '../components/common/Modal'

const MainPage = () => {
    return (
        <div className="mainPageLayout">
            <Modal />
            <MainPageHeader />
            <MainPageBody />
        </div>
    )
}

export default MainPage
