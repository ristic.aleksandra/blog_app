import React from 'react'
import AppMessageSection from '../components/AppMessagesSection'
import BlogCategories from '../components/BlogCategories'
import Blogs from '../components/Blogs'

const MainPageBody = () => {

    return (
        <div className="mainPageBody">
            <AppMessageSection />
            <BlogCategories />
            <Blogs />
        </div>
    )
}

export default MainPageBody
