import React from 'react'
import Search from '../components/common/Search'
import NavigationWeb from '../components/navigation/NavigationWeb'
import NavigationMobile from '../components/navigation/NavigationMobile'

const MainPageHeader = () => {

    const blogName = 'My Blog'

    return (
        <div className='mainPageHeader'>
            <div className='headerLabel'>
                <h4 className='header'>{blogName}</h4>
            </div>
            <div className='headerNavigation'>
                <Search />
                <NavigationMobile /> 
                <NavigationWeb />
            </div>
        </div>
    )
}

export default MainPageHeader
