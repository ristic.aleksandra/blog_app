import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {
    searchBlogPosts,
    fetchBlogPosts,
} from '../../../store/actions/blogPostsActions'

const Search = () => {
    const [searchValue, setSearchValue] = useState('')
    const dispach = useDispatch()

    const message = useSelector((state) => state.blogPosts.message)

    useEffect(() => {
        searchValue
            ? dispach(searchBlogPosts(searchValue))
            : dispach(fetchBlogPosts())
    }, [searchValue])

    useEffect(() => {
        setSearchValue('')
    }, [message])

    const handleSearchValueChanged = (value) => {
        setSearchValue(value)
    }

    return (
        <div className="search">
            <input
                value={searchValue}
                placeholder="Search"
                onChange={(e) => handleSearchValueChanged(e.target.value)}
            ></input>
        </div>
    )
}

export default Search
