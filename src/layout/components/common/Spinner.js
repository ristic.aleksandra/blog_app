import React from 'react'

import Loader from 'react-loader-spinner'

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

const Spinner = () => {
    return (
        <div className='spinnerDefault'>
            <label className="loafingMessage">Fetching Blog Posts, please wait...</label>
            <Loader
                type="Circles"
                color="black" 
            />
        </div>
    )
}

export default Spinner
