import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const AppMessage = () => {
    const [isMessageBoxOpen, setMessageBoxOpened] = useState(false)
    const message = useSelector((state) => state.blogPosts.message)

    const showHideClassName = isMessageBoxOpen ? 'displayFlex' : 'displayNone'

    useEffect(() => {
        const isMessage = message ? true : false
        setMessageBoxOpened(isMessage)
    }, [message])

    const handleCloseMessageBox = () => {
        setMessageBoxOpened(false)
    }

    return (
        <div className={`messageDefault ${showHideClassName}`}>
            <label className="messageDefaultTextLabel">{message}</label>
            <FontAwesomeIcon
                className="messageDefaultCloseButton"
                icon={faTimes}
                onClick={handleCloseMessageBox}
            />
        </div>
    )
}

export default AppMessage
