import React, { useState, useContext, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faSquare,
    faWindowMinimize,
    faMinusSquare,
} from '@fortawesome/free-regular-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types'

import Button from './Button'
import ModalContextProvider, { ModalContext } from '../../context/Context'

import {
  
    createBlogPost,
    updateBlogPost,
} from '../../../store/actions/blogPostsActions'

const Modal = () => {
    const [isNormalView, setNormalView] = useState(true)
    const [isMinimized, setMinimized] = useState(false)
    const [modalTitle, setModalTitle] = useState('')
    const [modalText, setModalText] = useState('')

    const { isModalOpen, setModalOpen } = useContext(ModalContext)

    const dispatch = useDispatch()

    const selectedBlog = useSelector((state) => state.blogPosts.selectedBlog)

    const showHideClassName = isModalOpen ? 'displayBlock' : 'displayNone'

    const showModalViewClassName = isNormalView
        ? 'modalViewNormal'
        : 'modalViewLarge'
    const buttonStyle = { width: '5rem', fontFamily:'cursive' }

    const modalViewIcon = isNormalView ? faSquare : faMinusSquare

    useEffect(() => {
        const { title, text } = selectedBlog
        setModalTitle(title)
        setModalText(text)
    }, [selectedBlog])

    const handleCloseModal = () => {
        setModalOpen(false)
        setModalText('')
        setModalTitle('')
    }

    const handleModalView = () => {
        setNormalView(!isNormalView)
        if (isMinimized) {
            setMinimized(false)
        }
    }

    const handleModalMinimize = () => {
        setMinimized(true)
    }

    const handleModalTitleChanged = (value) => {
        setModalTitle(value)
    }

    const handleModalTextChanged = (value) => {
        setModalText(value)
    }

    const handleCreateUpdateBlogPost = (e) => {
        e.preventDefault()
        const blogPost = {
            ...selectedBlog,
            title: modalTitle,
            text: modalText,
        }
        const { id } = blogPost

        id
            ? dispatch(updateBlogPost(blogPost))
            : dispatch(createBlogPost(blogPost))
        handleCloseModal()
    }

    const ModalHeader = ({ className }) => (
        <div className={className}>
            <h5 className='modalHeaderTitle'>Add/Edit blog post</h5>
            <div className='modalIcons'>
                {!isMinimized ? (
                    <FontAwesomeIcon
                        className='modalIcon'
                        icon={faWindowMinimize}
                        onClick={handleModalMinimize}
                    />
                ) : null}
                <FontAwesomeIcon
                    className='modalIcon'
                    icon={modalViewIcon}
                    onClick={handleModalView}
                />
                <FontAwesomeIcon
                    className='modalIcon'
                    icon={faTimes}
                    onClick={handleCloseModal}
                />
            </div>
        </div>
    )

    //!!!Note: Need to move all parts into one, because two way binding is not working when component splits into separate functions
    return (
        <ModalContextProvider>
            {!isMinimized ? (
                <div className={`modal ${showHideClassName}`}>
                    <div className={showModalViewClassName}>
                        <ModalHeader className='modalHeader' />
                        <div className='modalContentDefault'>
                            <form
                                className='modalContent'
                                type='submit'
                                onSubmit={(e)=>handleCreateUpdateBlogPost(e)}
                            >
                                <div className='modalTitle'>
                                    <label className='modalLabelText'>Title</label>
                                    <input
                                        className='modalFontText'
                                        value={modalTitle}
                                        type='text'
                                        required
                                        placeholder='Title of the post'
                                        onChange={(e) => handleModalTitleChanged(e.target.value)}
                                    ></input>
                                    <label className='modalLabelRequired'>*</label>
                                </div>
                                <div className='modalText'>
                                    <label className='modalLabelText'>Text</label>
                                    <textarea
                                        className={modalText?'modalFontText':'modalTextAreaAlignment modalFontText' }
                                        value={modalText}
                                        required
                                        placeholder='Text of the post'
                                        onChange={(e) => handleModalTextChanged(e.target.value)}
                                    ></textarea>
                                    <label className='modalLabelRequired'>*</label>
                                </div>
                                <div className='modalButtons'>
                                    <Button type='submit' buttonStyle={buttonStyle} text='Post' />
                                    <Button type='button' buttonStyle={buttonStyle} text='Cancel' handleButtonClicked={handleCloseModal} />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            ) : (
                <ModalHeader
                    className={isModalOpen ? 'minimizedModal' : showHideClassName}
                />
            )}
        </ModalContextProvider>
    )
}

Modal.propTypes = {
    className: PropTypes.string,
}

export default Modal
