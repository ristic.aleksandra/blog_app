import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ text, style, buttonStyle, handleButtonClicked, type }) => {
    return (
        <div style={style}>
            <button
                type={type}
                style={buttonStyle}
                className="buttonDefault"
                onClick={handleButtonClicked}
            >
                {text}
            </button>
        </div>
    )
}

Button.propTypes = {
    type: PropTypes.string,
    text: PropTypes.string.isRequired,
    style: PropTypes.any,
    buttonStyle: PropTypes.any,
    handleButtonClicked: PropTypes.func,
}

export default Button
