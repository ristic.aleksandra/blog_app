import React from 'react'
import PropTypes from 'prop-types'

const Navigation = ({ navItems, navItemClassName, navClassName }) => {
    return (
        <nav className={navClassName}>
            {navItems.map((navItem) => (
                <a className={navItemClassName} key={navItem.id} href="/">
                    {navItem.name}
                </a>
            ))}
        </nav>
    )
}

Navigation.propTypes = {
    navItems: PropTypes.array.isRequired,
    navItemClassName: PropTypes.any,
    navClassName: PropTypes.any,
}

export default Navigation
