import React, { useEffect, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { fetchBlogPosts } from '../../store/actions/blogPostsActions'

import Blog from '../components/Blog'
import Spinner from '../components/common/Spinner'

const Blogs = () => {
    const blogPosts = useSelector((state) => state.blogPosts.blogPosts)
    const message = useSelector((state) => state.blogPosts.message)
    const dispatch = useDispatch()

    const { loading } = blogPosts

    const items = Object.values(blogPosts.items)
    const emptyBlogList = items && items.length > 0 ? false : true

    useEffect(() => {
        dispatch(fetchBlogPosts())
    }, [message])

    
    const BlogItems = useMemo(
        () => (
            <div className="blogs">
                {items.map((blog) => (
                    <Blog key={blog.id} blog={blog} />
                ))}
            </div>
        ),
        [blogPosts]
    )

    const EmptyBlogs = () => <div>There is no Blog Posts!</div>

    return emptyBlogList ? <EmptyBlogs />: loading? <Spinner /> : <>{BlogItems}</>
}

export default Blogs
