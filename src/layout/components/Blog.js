import React, { useContext, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'
import Button from '../components/common/Button'
import { ModalContext } from '../context/Context'

import {
    getBlogPostById,
    deleteBlogPost,
} from '../../store/actions/blogPostsActions'

import {
    blogImageSrc,
    blogDescriptionImgsSrc,
} from '../../defaults/defaultData'

const Blog = ({ blog }) => {
    const { id, key, title, updatedAt, text } = blog

    const author = `Some Person${key}`

    const blogBackground = !(key % 2) ? 'blogLightGrey' : 'blogWhite'
    const style = { marginLeft: '1rem' }
    const buttonStyle = { width: '5rem', height:'1.2rem' }

    const modalContext = useContext(ModalContext)

    const dispatch = useDispatch()

    const blogPost = useSelector((state) => state.blogPosts.blogPosts.items[id])

    const handleOpenModal = () => {
        dispatch(getBlogPostById(id))
        modalContext.setModalOpen(true)
    }

    const handleDeleteBlogPost = (id) => {
        dispatch(deleteBlogPost(id))
    }

    const BlogHeader = () => (
        <div className="blogHeader">
            <img key={id} className="blogImg" src={blogImageSrc} />
            <div className="blogHeaderDetails">
                <h4 className="blogName">{title}</h4>
                <label className="authorAndDate">
          Posted date: {updatedAt} by {author}
                </label>
            </div>
        </div>
    )

    const BlogButtons = () => (
        <div className="blogButtons">
            <Button
                style={style}
                buttonStyle={buttonStyle}
                text="Edit"
                handleButtonClicked={() => handleOpenModal()}
            ></Button>
            <Button
                style={style}
                buttonStyle={buttonStyle}
                text="Delete"
                handleButtonClicked={() => handleDeleteBlogPost(id)}
            ></Button>
        </div>
    )

    const BlogText = () => (
        <div className="blogText">
            <p>{text}</p>
        </div>
    )

    const BlogImages = () => (
        <div className="blogImages">
            {blogDescriptionImgsSrc.map((img) => (
                <img className="blogDescriptionImg" key={img.id} src={img.src} />
            ))}
        </div>
    )

    const Blog = useMemo(
        () => (
            <div className={`blog ${blogBackground}`} key={id}>
                <div className="blogDetails">
                    <BlogHeader />
                    <BlogButtons />
                </div>
                <BlogText />
                <BlogImages />
            </div>
        ),
        [blogPost]
    )

    return <>{Blog}</>
}
Blog.propTypes = {
    blog: PropTypes.object.isRequired,
}

export default Blog
