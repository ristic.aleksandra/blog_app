import React, {useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Navigation from '../components/common/Navigation'

import { fetchCategories } from '../../store/actions/categoriesActions'

const BlogCategories = () => {
    const categories = useSelector((state) => state.categories.categories)
    const message = useSelector((state) => state.categories.message)
    const dispatch = useDispatch()

    const items = Object.values(categories.items)
    const emptyCategoryList = items && items.length > 0 ? false : true

    useEffect(() => {
        dispatch(fetchCategories())
    }, [message])

    const CategoryItems = () => (
        <div className="blogCategories">
            <div>
                <h3 className='blogCategoriesLabel'>Blog Categories</h3>
                <Navigation
                    navItems={items}
                    navItemClassName="categoryItem"
                    navClassName="categoryDefault"
                />
            </div>
        </div>
    )

    const EmptyCategories = () => <div>There is no Category yet!</div>

    return emptyCategoryList ? <EmptyCategories /> : <CategoryItems />
}

export default BlogCategories
