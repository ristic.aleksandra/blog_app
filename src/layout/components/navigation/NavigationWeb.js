import React from 'react'
import Navigation from '../common/Navigation'
import {navItems} from '../../../defaults/defaultData'

const NavigationWeb = () => {
    return (
        <div className="navigationWeb">
            <Navigation navItems={navItems} navItemClassName='navigationItem' />
        </div>
    )
}

export default NavigationWeb
