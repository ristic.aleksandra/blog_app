import React, { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

import { navItems } from '../../../defaults/defaultData'

const NavigationMobile = () => {
    const [isOpen, setIsOpen] = useState(false)

    const handleNavButtonClicked = () => {
        setIsOpen(!isOpen)
    }

    const handleCloseNavItems = () =>{
        setIsOpen(false)
    }
    return (
        <div className="navigationMobile">
            <button type="button" className="navBtn" onClick={handleNavButtonClicked}>
                <FontAwesomeIcon className="navIcon" icon={faBars} size="10x" />
            </button>
            <div className= {isOpen ? 'menuDefault' : 'displayNone'}>          
                <ul className={isOpen ? 'navLinks showNav' : 'navLinks'}>
                    <div>
                        {navItems.map((navItem) => (
                            <li key={navItem.id} id={navItem.id} className="menuItem" href="/">
                                {navItem.name}
                            </li>
                        ))}
                    </div>
                    <FontAwesomeIcon className="navCloseIcon" style={{background:'black'}} icon={faTimes} size="3x" onClick={handleCloseNavItems} />
                </ul>
            </div>
        </div>
    )
}

export default NavigationMobile
