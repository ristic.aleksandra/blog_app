import React, { useContext } from 'react'
import Button from '../components/common/Button'
import AppMessage from '../components/common/AppMessage'
import { ModalContext } from '../context/Context'

const AppMessageSection = () => {
    const buttonText = 'Add Post'
    const buttonStyle = { width: '10rem', height: '1.5rem' }

    const modalContext = useContext(ModalContext)

    const handleOpenModal = () => {
        modalContext.setModalOpen(true)
    }

    return (
        <div className="appMessageSection">
            <div className="appMessage">
                <h2 onClick={handleOpenModal}>Welcome to My Blog</h2>
                <AppMessage />
                <Button
                    style={{ alignSelf: 'flex-end' }}
                    buttonStyle={buttonStyle}
                    text={buttonText}
                    handleButtonClicked={handleOpenModal}
                ></Button>
            </div>
        </div>
    )
}

export default AppMessageSection
