import axios from 'axios'
import BASE_URL from './index'

const CATEGORY_URL = `${BASE_URL}/Category`

const getCategories = () => {
    return axios.get(`${CATEGORY_URL}`)
}

export { getCategories }