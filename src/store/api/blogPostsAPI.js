import axios from 'axios'
import BASE_URL from './index'

const BLOG_POSTS_URL = `${BASE_URL}/BlogPosts`
const SEARCH_BLOG_POSTS_URL = `${BLOG_POSTS_URL}/Search`

const getBlogPosts = () => {
    return axios.get(`${BLOG_POSTS_URL}`)
}

const getBlogPostById = (id) => {
    return axios.get(`${BLOG_POSTS_URL}/${id}`)
}

const serachBlogPosts = (term) => {
    return axios.get(`${SEARCH_BLOG_POSTS_URL}?term=${term}`)
}

const createBlogPost = (blogPost) => {
    return axios.post(`${BLOG_POSTS_URL}`, blogPost)
}

const updateBlogPost = (blogPost) => {
    return axios.put(`${BLOG_POSTS_URL}/${blogPost.id}`, blogPost)
}

const deleteBlogPost = (id) => {
    return axios.delete(`${BLOG_POSTS_URL}/${id}`)
}


export { getBlogPosts, createBlogPost, getBlogPostById, updateBlogPost, deleteBlogPost, serachBlogPosts }
