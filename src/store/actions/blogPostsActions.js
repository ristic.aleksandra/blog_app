import {FETCH_BLOG_POSTS, ADD_BLOG_POST, GET_BLOG_BY_ID, UPDATE_BLOG_POST, DELETE_BLOG_POST, SEARCH_BLOG_POSTS} from '../actionTypes/blogPostsActionTypes'

const fetchBlogPosts = () => {
    return {
        type: FETCH_BLOG_POSTS,
    }
}

const getBlogPostById = (id) => {
    return {
        type: GET_BLOG_BY_ID,
        id
    }
}

const deleteBlogPost = (id) => {
    return {
        type: DELETE_BLOG_POST,
        id
    }
}

const createBlogPost = (blogPost) => {
    return {
        type: ADD_BLOG_POST,
        blogPost
    }
}

const updateBlogPost = (blogPost) => {
    return {
        type: UPDATE_BLOG_POST,
        blogPost
    }
}

const searchBlogPosts = (term) => {
    return {
        type: SEARCH_BLOG_POSTS,
        term
    }
}
export { fetchBlogPosts, createBlogPost, getBlogPostById, updateBlogPost, deleteBlogPost, searchBlogPosts }
