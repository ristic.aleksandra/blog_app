import {FETCH_CATEGORIES} from '../actionTypes/categoriesActionTypes'

const fetchCategories = () => {
    return {
        type: FETCH_CATEGORIES,
    }
}

export { fetchCategories }