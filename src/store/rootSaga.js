import { all, call } from 'redux-saga/effects'
import blogPostWatcher from './sagas/blogPostSagas'
import categoriesWatcher from './sagas/categoriesSagas'

export default function* rootSaga() {
    yield all([call(blogPostWatcher), call(categoriesWatcher)])
}
