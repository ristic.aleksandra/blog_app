import * as actionsTypes from '../actionTypes/blogPostsActionTypes'

const initialSelectedBlog = { id: 0, title: '', text: '', categoryId: 0 }
const initialMessage = ''
const initialState = {
    blogPosts: { loading: true, items: {} },
    message:initialMessage,
    selectedBlog: initialSelectedBlog,
}

const blogPostsReducer = (state = initialState, action) => {
    switch (action.type) {
    case actionsTypes.FETCH_BLOG_POSTS:
    case actionsTypes.SEARCH_BLOG_POSTS:
        return {
            ...state,
            blogPosts: {
                ...state.blogPosts,
                loading: true,
            },
        }
    case actionsTypes.SEARCH_BLOG_POSTS_SUCCESS:
    case actionsTypes.SEARCH_BLOG_POSTS_FAILURE:
    case actionsTypes.FETCH_BLOG_POSTS_SUCCESS:
    case actionsTypes.FETCH_BLOG_POSTS_FAILURE:
        return {
            ...state,
            blogPosts: {
                ...state.blogPosts,
                loading: false,
                items: action.payload.items,
            },
        }

    case actionsTypes.GET_BLOG_BY_ID_SUCCESS:
        return {
            ...state,
            selectedBlog: action.payload.selectedBlog,
        }
    case actionsTypes.GET_BLOG_BY_ID_FAILURE:
        return {
            ...state,
            message: action.payload.message,
        }

    case actionsTypes.ADD_BLOG_POST:
    case actionsTypes.DELETE_BLOG_POST:
    case actionsTypes.UPDATE_BLOG_POST:
        return {
            ...state,
            message: initialMessage,
        }

    case actionsTypes.DELETE_BLOG_POST_SUCCESS:
    case actionsTypes.DELETE_BLOG_POST_FAILURE:
    case actionsTypes.ADD_BLOG_POST_SUCCESS:
    case actionsTypes.ADD_BLOG_POST_FAILURE:
    case actionsTypes.UPDATE_BLOG_POST_SUCCESS:
    case actionsTypes.UPDATE_BLOG_POST_FAILURE:
        return {
            ...state,
            selectedBlog:initialSelectedBlog,
            message: action.payload.message,
        }

    default:
        return state
    }
}

export default blogPostsReducer
