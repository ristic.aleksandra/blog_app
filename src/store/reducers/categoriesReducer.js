import * as actionsTypes from '../actionTypes/categoriesActionTypes'

const initialState = {
    categories: { loading: true, items: {} }
}

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
    case actionsTypes.FETCH_CATEGORIES:
        return {
            ...state,
            categories: {
                ...state.categories,
                loading: true,
            },
        }

    case actionsTypes.FETCH_CATEGORIES_SUCCESS:
    case actionsTypes.FETCH_CATEGORIES_FAILURE:
        return {
            ...state,
            categories: {
                ...state.categories,
                loading: false,
                items: action.payload.items,
            },
        }

    default:
        return state
    }
}

export default categoriesReducer