import { createStore, applyMiddleware, combineReducers } from 'redux'
import blogPostsReducer from './reducers/blogPostsReducer'
import categoriesReducer from './reducers/categoriesReducer'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './rootSaga'

const rootReducer = combineReducers({
    blogPosts: blogPostsReducer,
    categories: categoriesReducer,
})

const sagaMiddleware = createSagaMiddleware()

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

export default store

sagaMiddleware.run(rootSaga)
