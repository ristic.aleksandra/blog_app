import { put, call, takeLatest } from 'redux-saga/effects'
import {
    FETCH_CATEGORIES,
    FETCH_CATEGORIES_SUCCESS,
    FETCH_CATEGORIES_FAILURE,
} from '../actionTypes/categoriesActionTypes'
import { getCategories } from '../API/categoriesAPI'

import 'regenerator-runtime/runtime'

function* handleFetchCategories() {
    try {
        const data = yield call(getCategories)
        const { resultData } = data.data
        const updatedCategories =[...resultData]
        var updatedCategoriesDataMap = updatedCategories.reduce((map, obj) => ({
            ...map,
            [obj.id] : obj
        }), {})
        yield put({
            type: FETCH_CATEGORIES_SUCCESS,
            payload: { items: updatedCategoriesDataMap },
        })
    } catch (error) {
        yield put({ type: FETCH_CATEGORIES_FAILURE, payload: { items: [] } })
    }
}

function* categoriesWatcher() {
    yield takeLatest(FETCH_CATEGORIES, handleFetchCategories)
}

export default categoriesWatcher
