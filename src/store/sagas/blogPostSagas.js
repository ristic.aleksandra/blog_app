import { put, call, takeLatest } from 'redux-saga/effects'
import {
    FETCH_BLOG_POSTS,
    FETCH_BLOG_POSTS_SUCCESS,
    FETCH_BLOG_POSTS_FAILURE,
    ADD_BLOG_POST,
    ADD_BLOG_POST_SUCCESS,
    ADD_BLOG_POST_FAILURE,
    GET_BLOG_BY_ID,
    GET_BLOG_BY_ID_SUCCESS,
    GET_BLOG_BY_ID_FAILURE,
    UPDATE_BLOG_POST,
    UPDATE_BLOG_POST_SUCCESS,
    UPDATE_BLOG_POST_FAILURE,
    DELETE_BLOG_POST,
    DELETE_BLOG_POST_SUCCESS,
    DELETE_BLOG_POST_FAILURE,
    SEARCH_BLOG_POSTS,
    SEARCH_BLOG_POSTS_SUCCESS,
    SEARCH_BLOG_POSTS_FAILURE
} from '../actionTypes/blogPostsActionTypes'
import { getBlogPosts, createBlogPost, getBlogPostById, updateBlogPost, deleteBlogPost, serachBlogPosts } from '../API/blogPostsAPI'

import moment from 'moment'

import 'regenerator-runtime/runtime'

function transformResultData(resultData){
    let updatedResultData = [...resultData]

    if (updatedResultData && updatedResultData.length > 0) {
        updatedResultData.map((result) => {
            let { updatedAt } = result
            var utc = moment.utc(updatedAt).toDate()
            result.updatedAt = `${moment(utc)
                .format('DD.MM.YYYY')} at ${moment(utc).format('HH:mm')}`
        })
    }

    return updatedResultData.reduce((map, obj, index) => ({
        ...map,
        [obj.id] : {...obj, 'key':index}
    }), 0)
}

function* handleFetchBlogPosts() {
    try {
        const data = yield call(getBlogPosts)
        const { resultData } = data.data
      
        const updatedResultDataMap = transformResultData(resultData)

        yield put({
            type: FETCH_BLOG_POSTS_SUCCESS,
            payload: { items: updatedResultDataMap },
        })
    } catch (error) {
        yield put({ type: FETCH_BLOG_POSTS_FAILURE, payload: { items: [] } })
    }
}

function* handleGetBlogById({id}) {
    try {
        const data = yield call(getBlogPostById, id)
        const {resultData} = data.data
  
        yield put({
            type: GET_BLOG_BY_ID_SUCCESS,
            payload: { selectedBlog: resultData },
        })
    } catch (error) {
        yield put({ type:GET_BLOG_BY_ID_FAILURE, payload: { message: error } })
    }
}

function* handleSearchBlogPosts({term}) {
    try {
        const data = yield call(serachBlogPosts, term)
        const { resultData } = data.data
        const updatedResultDataMap = transformResultData(resultData)

        yield put({
            type: SEARCH_BLOG_POSTS_SUCCESS,
            payload: { items: updatedResultDataMap },
        })
    } catch (error) {
        yield put({ type: SEARCH_BLOG_POSTS_FAILURE, payload: { items: [] } })
    }
}

function* handleCreateBlogPosts({ blogPost }) {
    try {
        yield call(createBlogPost, blogPost)
        yield put({
            type:  ADD_BLOG_POST_SUCCESS,
            payload: { message: 'Blog Post has been successfully added.' },
        })

    } catch (error) {
        console.log(error, 'error')
        yield put({ type: ADD_BLOG_POST_FAILURE , payload: { message: error } })
    }
}
function* handleUpdateBlogPost({ blogPost }) {
    try {
        yield call(updateBlogPost, blogPost)
        yield put({
            type: UPDATE_BLOG_POST_SUCCESS,
            payload: { message: 'Blog Post has been successfully updated.' },
        })

    } catch (error) {
        console.log(error, 'error')
        yield put({ type:UPDATE_BLOG_POST_FAILURE , payload: { message: error } })
    }
}

function* handleDeleteBlogPost({id}) {
    try {
        yield call(deleteBlogPost, id)  
        yield put({
            type: DELETE_BLOG_POST_SUCCESS,
            payload: { message: 'Blog Post has been successfully deleted.' },
        })
    } catch (error) {
        yield put({ type:DELETE_BLOG_POST_FAILURE, payload: { message: error } })
    }
}

function* blogPostWatcher() {
    yield takeLatest(FETCH_BLOG_POSTS, handleFetchBlogPosts)
    yield takeLatest(ADD_BLOG_POST, handleCreateBlogPosts)
    yield takeLatest(GET_BLOG_BY_ID, handleGetBlogById)
    yield takeLatest(UPDATE_BLOG_POST, handleUpdateBlogPost)
    yield takeLatest(DELETE_BLOG_POST, handleDeleteBlogPost)
    yield takeLatest(SEARCH_BLOG_POSTS, handleSearchBlogPosts)
}

export default blogPostWatcher
