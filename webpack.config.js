const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: path.resolve(__dirname, 'src/index'),
    output: {
        path: path.resolve(__dirname),
        filename: 'bundle.js'
    },
    module: {   
        rules: [    
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
            },
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src'),
                use: ['babel-loader', ]
            },{
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader' 
                }, {
                    loader: 'sass-loader'
                },
                ]},
            {
                test: /\.(jpg|png)$/,
                include: path.resolve(__dirname, 'src/public'),
                use: {
                    loader: 'url-loader',
                    loader: 'file-loader' 
                },
            },
      
        ]
    },
    devServer: {
        contentBase:  path.resolve(__dirname),
        port: 3000
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html' 
        })
    ]
}