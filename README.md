# Blog App

Blog App is application for adding, updating and deleting blog posts.

## Installation

Use npm or yarn to install package dependencies.

```bash
npm install 
```

## Starting the App

Use npm or yarn to run the application.

```bash
npm start
```

## Functionalities
 
 1. Adding new Blog
 2. Deleting Blogs
 3. Updating Blogs
 4. Searhing Blogs by title or text
 5. Displaying Blogs (all/serached)
 6. Dispalying categories
 7. Modal functionalities (open, close, minimaze, maximize Modal)
 

